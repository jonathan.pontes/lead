package com.br.lead.LeadCollector.controllers;

import com.br.lead.LeadCollector.models.Lead;
import com.br.lead.LeadCollector.models.Produto;
import com.br.lead.LeadCollector.models.Usuario;
import com.br.lead.LeadCollector.security.DetalhesUsuario;
import com.br.lead.LeadCollector.security.JWTUtil;
import com.br.lead.LeadCollector.services.ProdutoService;
import com.br.lead.LeadCollector.services.UsuarioService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Arrays;
import java.util.Optional;

@WebMvcTest(ProdutoController.class)
public class ProdutoControllerTest {

    @MockBean
    ProdutoService produtoService;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UsuarioService usuarioService;

    @MockBean
    private JWTUtil jwtUtil;

    ObjectMapper mapper = new ObjectMapper();

    @BeforeEach
    public void init() {
        Usuario usuario = new Usuario(1, "Joao", "joao@joao", "123");
        DetalhesUsuario detalhesUsuario = new DetalhesUsuario(usuario.getId(), usuario.getEmail(), usuario.getSenha());
        Mockito.when(usuarioService.loadUserByUsername(Mockito.anyString())).thenReturn(detalhesUsuario);

        Mockito.when(jwtUtil.getUsername(Mockito.anyString())).thenReturn("joao@joao");
        Mockito.when(jwtUtil.tokenValido(Mockito.anyString())).thenReturn(true);
    }

    @Test
    public void deveBuscarTodosOsProdutos() throws Exception {
        Iterable<Produto> iterableProdutos = Arrays.asList(
                criarProduto(1, "Caderno"),
                criarProduto(2, "Lapis"),
                criarProduto(3, "Caneta"));

        Mockito.when(produtoService.buscarTodos()).thenReturn(iterableProdutos);

        String json = mapper.writeValueAsString(iterableProdutos);

        mockMvc.perform(MockMvcRequestBuilders.get("/produtos")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.hasSize(3)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].nome", CoreMatchers.equalTo("Caderno")))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].nome", CoreMatchers.equalTo("Lapis")))
                .andExpect(MockMvcResultMatchers.jsonPath("$[2].nome", CoreMatchers.equalTo("Caneta")));
    }

    @Test
    public void deveCadastrarProduto() throws Exception {
        Produto produto = criarProduto(1, "Caderno");
        Mockito.when(produtoService.cadastrar(Mockito.any(Produto.class))).thenReturn(produto);

        String json = mapper.writeValueAsString(produto);

        mockMvc.perform(MockMvcRequestBuilders.post("/produtos")
                .contentType(MediaType.APPLICATION_JSON)
                .header("Authorization", "Bearer " + "DevePassar")
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.nome", CoreMatchers.equalTo("Caderno")));
    }

    @Test
    public void deveBuscarProdutoPorId() throws Exception {
        Produto produto = criarProduto(7, "Caneta");
        Mockito.when(produtoService.buscarPorId(Mockito.anyInt())).thenReturn(Optional.of(produto));

        String json = mapper.writeValueAsString(produto);

        mockMvc.perform(MockMvcRequestBuilders.get("/produtos/7")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.nome", CoreMatchers.equalTo("Caneta")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(7)));
    }

    @Test
    public void deveBuscarProdutoPorIdEFalharNaCansulta() throws Exception {
        Mockito.when(produtoService.buscarPorId(Mockito.anyInt())).thenReturn(Optional.empty());

        mockMvc.perform(MockMvcRequestBuilders.get("/produtos/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(""))
                .andExpect(MockMvcResultMatchers.status().isNoContent());
    }

    @Test
    public void deveAtualizarProduto() throws Exception {
        Produto produto = criarProduto(1, "Caderno");

        Mockito.when(produtoService.atualizar(Mockito.any(Produto.class))).thenReturn(produto);

        String json = mapper.writeValueAsString(produto);

        mockMvc.perform(MockMvcRequestBuilders.put("/produtos/1")
                .contentType(MediaType.APPLICATION_JSON)
                .header("Authorization", "Bearer " + "DevePassar")
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.nome", CoreMatchers.equalTo("Caderno")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(1)));
    }

    @Test
    public void deveDeveDeletarProdutoPorId() throws Exception {
        Produto produto = criarProduto(3, "Lapis");

        Mockito.when(produtoService.buscarPorId(Mockito.anyInt())).thenReturn(Optional.of(produto));

        String json = mapper.writeValueAsString(produto);

        mockMvc.perform(MockMvcRequestBuilders.delete("/produtos/3")
                .contentType(MediaType.APPLICATION_JSON)
                .header("Authorization", "Bearer " + "DevePassar")
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(3)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.nome", CoreMatchers.equalTo("Lapis")));

        Mockito.verify(produtoService, Mockito.times(1)).deletar(produto);
    }

    @Test
    public void deveDeveDarErroAoTentarDeletarProdutoPorIdQueNaoExisteNoBancoDeDados() throws Exception {
        Produto produto = criarProduto(3, "Lapis");
        Mockito.when(produtoService.buscarPorId(Mockito.anyInt())).thenReturn(Optional.empty());

        String json = mapper.writeValueAsString(produto);

        mockMvc.perform(MockMvcRequestBuilders.delete("/produtos/1")
                .contentType(MediaType.APPLICATION_JSON)
                .header("Authorization", "Bearer " + "DevePassar")
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }


    private Produto criarProduto(int id, String nome) {
        Produto produto = new Produto();
        produto.setNome(nome);
        produto.setId(id);
        produto.setPreco(10.00);
        produto.setDescricao("Café da fazenda boa vista. ");
        return produto;
    }

}
