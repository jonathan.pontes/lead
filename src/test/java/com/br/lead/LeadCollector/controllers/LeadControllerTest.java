package com.br.lead.LeadCollector.controllers;

import com.br.lead.LeadCollector.enums.TipoDeLead;
import com.br.lead.LeadCollector.models.Lead;
import com.br.lead.LeadCollector.models.Produto;
import com.br.lead.LeadCollector.models.Usuario;
import com.br.lead.LeadCollector.security.DetalhesUsuario;
import com.br.lead.LeadCollector.security.JWTUtil;
import com.br.lead.LeadCollector.services.LeadService;
import com.br.lead.LeadCollector.services.UsuarioService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.web.context.WebApplicationContext;

import java.util.Arrays;
import java.util.Optional;

@WebMvcTest(LeadController.class)
public class LeadControllerTest {

    @MockBean
    LeadService leadService;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UsuarioService usuarioService;

    @MockBean
    private JWTUtil jwtUtil;

    ObjectMapper mapper = new ObjectMapper();

    @BeforeEach
    public void init() {
        Usuario usuario = new Usuario(1, "Joao", "joao@joao", "123");
        DetalhesUsuario detalhesUsuario = new DetalhesUsuario(usuario.getId(), usuario.getEmail(), usuario.getSenha());
        Mockito.when(usuarioService.loadUserByUsername(Mockito.anyString())).thenReturn(detalhesUsuario);

        Mockito.when(jwtUtil.getUsername(Mockito.anyString())).thenReturn("joao@joao");
        Mockito.when(jwtUtil.tokenValido(Mockito.anyString())).thenReturn(true);
    }


    @Test
    public void deveBuscarTodosOsLeads() throws Exception {
        Iterable<Lead> iterableLeads = Arrays.asList(
                criarLead(new Produto(), 1, "Joselito"),
                criarLead(new Produto(), 2, "Overchico"),
                criarLead(new Produto(), 3, "Dollynho"));

        Mockito.when(leadService.buscarTodosLeads()).thenReturn(iterableLeads);

        String json = mapper.writeValueAsString(iterableLeads);

        mockMvc.perform(MockMvcRequestBuilders.get("/leads")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.hasSize(3)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].nome", CoreMatchers.equalTo("Joselito")))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].nome", CoreMatchers.equalTo("Overchico")))
                .andExpect(MockMvcResultMatchers.jsonPath("$[2].nome", CoreMatchers.equalTo("Dollynho")));
    }

    @Test
    public void testarCadastroDeLead() throws Exception {

        Produto produto = criarProduto(1, "caderno");
        Lead lead = criarLead(produto, 0, "Joselito");

        Iterable<Produto> produtosIterable = Arrays.asList(produto);

        Mockito.when(leadService.salvarLead(Mockito.any(Lead.class))).thenReturn(lead);
        Mockito.when(leadService.buscarTodosProdutos(Mockito.anyList())).thenReturn(produtosIterable);

        String json = mapper.writeValueAsString(lead);

        mockMvc.perform(MockMvcRequestBuilders.post("/leads")
                .contentType(MediaType.APPLICATION_JSON)
                .header("Authorization", "Bearer " + "DevePassar")
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.produtos[0].id", CoreMatchers.equalTo(1)));
    }

    @Test
    public void deveBuscarLeadPorId() throws Exception {
        Lead lead = criarLead(new Produto(), 1, "Joselito");
        Mockito.when(leadService.buscarPorId(Mockito.anyInt())).thenReturn(Optional.of(lead));

        String json = mapper.writeValueAsString(lead);

        mockMvc.perform(MockMvcRequestBuilders.get("/leads/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.nome", CoreMatchers.equalTo("Joselito")));
    }

    @Test
    public void deveBuscarLeadPorIdEFalharNaCansulta() throws Exception {
        Mockito.when(leadService.buscarPorId(Mockito.anyInt())).thenReturn(Optional.empty());

        mockMvc.perform(MockMvcRequestBuilders.get("/leads/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(""))
                .andExpect(MockMvcResultMatchers.status().isNoContent());
    }

    @Test
    public void deveAtualizarLead() throws Exception {
        Produto produto = criarProduto(1, "caderno");
        Lead lead = criarLead(produto, 1, "Joselito");

        Mockito.when(leadService.atualizarLead(Mockito.any(Lead.class))).thenReturn(lead);

        String json = mapper.writeValueAsString(lead);

        mockMvc.perform(MockMvcRequestBuilders.put("/leads/1")
                .contentType(MediaType.APPLICATION_JSON)
                .header("Authorization", "Bearer " + "DevePassar")
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.produtos[0].nome", CoreMatchers.equalTo("caderno")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.nome", CoreMatchers.equalTo("Joselito")));
    }

    @Test
    public void deveDeveDeletarLeadPorId() throws Exception {
        Lead lead = criarLead(new Produto(), 1, "Joselito");

        Mockito.when(leadService.buscarPorId(Mockito.anyInt())).thenReturn(Optional.of(lead));

        String json = mapper.writeValueAsString(lead);

        mockMvc.perform(MockMvcRequestBuilders.delete("/leads/1")
                .contentType(MediaType.APPLICATION_JSON)
                .header("Authorization", "Bearer " + "DevePassar")
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.nome", CoreMatchers.equalTo("Joselito")));

        Mockito.verify(leadService, Mockito.times(1)).deletarLead(lead);
    }

    @Test
    public void deveDeveDarErroAoTentarDeletarLeadPorIdQueNaoExisteNoBancoDeDados() throws Exception {
        Lead lead = criarLead(new Produto(), 1, "Joselito");

        Mockito.when(leadService.buscarPorId(Mockito.anyInt())).thenReturn(Optional.empty());

        String json = mapper.writeValueAsString(lead);

        mockMvc.perform(MockMvcRequestBuilders.delete("/leads/1")
                .contentType(MediaType.APPLICATION_JSON)
                .header("Authorization", "Bearer " + "DevePassar")
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }


    private Produto criarProduto(int id, String nome) {
        Produto produto = new Produto();
        produto.setNome(nome);
        produto.setId(id);
        produto.setPreco(10.00);
        produto.setDescricao("Café da fazenda boa vista. ");
        return produto;
    }

    private Lead criarLead(Produto produto, int id, String nome) {
        Lead lead = new Lead();
        if (id != 0)
            lead.setId(id);

        lead.setNome(nome);
        lead.setEmail("teste@gmail.com");
        lead.setTipoDeLead(TipoDeLead.QUENTE);
        lead.setProdutos(Arrays.asList(produto));
        return lead;
    }
}
