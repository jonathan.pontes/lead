package com.br.lead.LeadCollector.services;

import com.br.lead.LeadCollector.enums.TipoDeLead;
import com.br.lead.LeadCollector.models.Lead;
import com.br.lead.LeadCollector.models.Produto;
import com.br.lead.LeadCollector.repositories.LeadRepository;
import com.br.lead.LeadCollector.repositories.ProdutoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@SpringBootTest
public class LeadServiceTest {

    @MockBean
    LeadRepository leadRepository;

    @MockBean
    ProdutoRepository produtoRepository;

    @Autowired
    LeadService leadService;

    Lead lead;

    @BeforeEach
    public void inicializar() {
        lead = new Lead();
        lead.setId(1);
        lead.setTipoDeLead(TipoDeLead.QUENTE);
        lead.setEmail("vinicius@gmail.com");
        lead.setNome("Vinicius");
        lead.setProdutos(Arrays.asList(new Produto()));
    }

    @Test
    public void testarSalvarLead() {

        Mockito.when(leadRepository.save(Mockito.any(Lead.class))).thenReturn(lead);

        Lead leadObjeto = leadService.salvarLead(lead);

        Assertions.assertEquals(lead, leadObjeto);
        Assertions.assertEquals(lead.getEmail(), leadObjeto.getEmail());
    }

    @Test
    public void testarBuscarTodosProdutos() {
        Iterable<Produto> produtoIterable = obterIterableDeProdutos();

        Mockito.when(produtoRepository.findAllById(Mockito.anyList())).thenReturn(produtoIterable);

        List<Integer> idsProdutos = Arrays.asList(1, 2);
        Iterable<Produto> produtosRetorno = leadService.buscarTodosProdutos(idsProdutos);
        List<Produto> listaDeProdutos = (List) produtosRetorno;

        Assertions.assertEquals(2, listaDeProdutos.size());
        Assertions.assertEquals(1, listaDeProdutos.get(0).getId());
        Assertions.assertEquals("caderno", listaDeProdutos.get(0).getNome());
        Assertions.assertEquals(2, listaDeProdutos.get(1).getId());
        Assertions.assertEquals("caneta", listaDeProdutos.get(1).getNome());
    }

    @Test
    public void testarBuscarPorId() {
        Optional<Lead> leadOptional = Optional.of(lead);

        Mockito.when(leadRepository.findById(Mockito.anyInt())).thenReturn(leadOptional);

        Optional<Lead> leadRetorno = leadService.buscarPorId(1);

        Assertions.assertEquals(1, leadRetorno.get().getId());
        Assertions.assertEquals("Vinicius", leadRetorno.get().getNome());
        Assertions.assertEquals(TipoDeLead.QUENTE, leadRetorno.get().getTipoDeLead());
    }

    @Test
    public void testarBuscarTodosLeads() {
        Iterable<Lead> leads = obterIterableDeLead() ;
        Mockito.when(leadRepository.findAll()).thenReturn(leads);

        Iterable<Lead> retorno = leadService.buscarTodosLeads();
        List<Lead> listaDeLead = (List<Lead>) retorno;

        Assertions.assertEquals(2, listaDeLead.size());
        Assertions.assertEquals(1, listaDeLead.get(0).getId());
        Assertions.assertEquals("João", listaDeLead.get(0).getNome());
        Assertions.assertEquals(2, listaDeLead.get(1).getId());
        Assertions.assertEquals("Maria", listaDeLead.get(1).getNome());
    }

    @Test
    public void testarAtualizarLeadComTodosOsCamposPreenchidos() {
        Optional<Lead> leadOptional = Optional.of(lead);
        Mockito.when(leadRepository.findById(Mockito.anyInt())).thenReturn(leadOptional);

        Mockito.when(leadRepository.save(Mockito.any(Lead.class))).thenReturn(lead);
        Lead leadObjeto = leadService.atualizarLead(lead);

        Assertions.assertEquals(leadObjeto, lead);
        Assertions.assertEquals(leadObjeto.getEmail(), lead.getEmail());
    }

    @Test
    public void testarAtualizarLeadComTodosOsCamposNulos() {
        Optional<Lead> leadOptional = Optional.of(lead);
        Mockito.when(leadRepository.findById(Mockito.anyInt())).thenReturn(leadOptional);

        Mockito.when(leadRepository.save(Mockito.any(Lead.class))).thenReturn(lead);
        Lead leadObjeto = leadService.atualizarLead(new Lead(1, null, null, null, null));

        Assertions.assertEquals(leadObjeto, lead);
        Assertions.assertEquals(leadObjeto.getEmail(), lead.getEmail());
    }

    @Test
    public void testeDeletarLead() {
        leadService.deletarLead(lead);
        Mockito.verify(leadRepository, Mockito.times(1)).delete(Mockito.any(Lead.class));
    }

    private Iterable<Produto> obterIterableDeProdutos() {
        return Arrays.asList(
                new Produto(1, "caderno", "ben 10", 10.0),
                new Produto(2, "caneta", "caneta azul", 1.0));
    }

    private Iterable<Lead> obterIterableDeLead() {
        return Arrays.asList(
                new Lead(1, "João", "joao@gmail.com", TipoDeLead.FRIO, Arrays.asList(new Produto())),
                new Lead(2, "Maria", "maria@gmail.com", TipoDeLead.QUENTE, Arrays.asList(new Produto()))
        );

    }
}
