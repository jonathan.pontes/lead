package com.br.lead.LeadCollector.services;

import com.br.lead.LeadCollector.models.Lead;
import com.br.lead.LeadCollector.models.Produto;
import com.br.lead.LeadCollector.repositories.ProdutoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@SpringBootTest
public class ProdutoServiceTest {

    @MockBean
    ProdutoRepository produtoRepository;

    @Autowired
    ProdutoService produtoService;

    Produto produto;

    @BeforeEach
    public void inicializar() {
        produto = new Produto(1, "caderno", "ben 10", 10.0);
    }

    @Test
    public void deveSalvarProduto() {
        Mockito.when(produtoRepository.save(Mockito.any(Produto.class))).thenReturn(produto);
        Produto retorno = produtoService.cadastrar(produto);

        Assertions.assertEquals(retorno, produto);
        Assertions.assertEquals(retorno.getId(), produto.getId());
    }

    @Test
    public void deveBuscarTodosProdutos() {
        Iterable<Produto> produtoIterable = obterIterableDeProdutos();

        Mockito.when(produtoRepository.findAll()).thenReturn(produtoIterable);

        Iterable<Produto> produtosRetorno = produtoService.buscarTodos();
        List<Produto> listaDeProdutos = (List) produtosRetorno;

        Assertions.assertEquals(2, listaDeProdutos.size());
        Assertions.assertEquals(1, listaDeProdutos.get(0).getId());
        Assertions.assertEquals("caderno", listaDeProdutos.get(0).getNome());
        Assertions.assertEquals(2, listaDeProdutos.get(1).getId());
        Assertions.assertEquals("caneta", listaDeProdutos.get(1).getNome());
    }

    @Test
    public void deveBuscarPorId() {
        Optional<Produto> produtoOptional = Optional.of(produto);
        Mockito.when(produtoRepository.findById(Mockito.anyInt())).thenReturn(produtoOptional);
        Optional<Produto> retorno = produtoService.buscarPorId(1);

        Assertions.assertEquals(1, retorno.get().getId());
        Assertions.assertEquals("caderno", retorno.get().getNome());
        Assertions.assertEquals(10.0, retorno.get().getPreco());
    }

    @Test
    public void deveAtualizarProdutoComTodosOsCamposPreenchidos() {
        Optional<Produto> optional = Optional.of(produto);
        Mockito.when(produtoRepository.findById(Mockito.anyInt())).thenReturn(optional);
        Mockito.when(produtoRepository.save(Mockito.any(Produto.class))).thenReturn(produto);

        Produto retorno = produtoService.atualizar(produto);
        Assertions.assertEquals(retorno, produto);
        Assertions.assertEquals(retorno.getId(), produto.getId());
    }

    @Test
    public void deveAtualizarProdutoComTodosOsCamposNulos() {
        Optional<Produto> optional = Optional.of(produto);
        Mockito.when(produtoRepository.findById(Mockito.anyInt())).thenReturn(optional);
        Mockito.when(produtoRepository.save(Mockito.any(Produto.class))).thenReturn(produto);

        Produto retorno = produtoService.atualizar(new Produto(1, null, null, null));
        Assertions.assertEquals(retorno, produto);
        Assertions.assertEquals(retorno.getId(), produto.getId());
    }


    @Test
    public void deveDeletarProduto() {
        produtoService.deletar(produto);
        Mockito.verify(produtoRepository, Mockito.times(1)).delete(Mockito.any(Produto.class));
    }

    private Iterable<Produto> obterIterableDeProdutos() {
        return Arrays.asList(
                new Produto(1, "caderno", "ben 10", 10.0),
                new Produto(2, "caneta", "caneta azul", 1.0));
    }
}
