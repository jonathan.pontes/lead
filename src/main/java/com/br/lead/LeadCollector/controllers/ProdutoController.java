package com.br.lead.LeadCollector.controllers;

import com.br.lead.LeadCollector.models.Produto;
import com.br.lead.LeadCollector.services.ProdutoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;

@RestController
@RequestMapping("/produtos")
public class ProdutoController {

    @Autowired
    private ProdutoService produtoService;

    @GetMapping
    public Iterable<Produto> buscarTodos() {
        return produtoService.buscarTodos();
    }

    @GetMapping("/{id}")
    public Produto buscarUnitariamente(@PathVariable Integer id) {
        Optional<Produto> optional = produtoService.buscarPorId(id);
        if (optional.isPresent()) {
            return optional.get();
        } else {
            throw new ResponseStatusException(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping
    public ResponseEntity<Produto> cadastrar(@RequestBody Produto produto) {
        Produto retorno = produtoService.cadastrar(produto);
        return ResponseEntity.status(201).body(retorno);
    }

    @PutMapping("/{id}")
    public Produto atualizar(@PathVariable Integer id, @RequestBody Produto produto) {
        produto.setId(id);
        Produto retorno = produtoService.atualizar(produto);
        return retorno;
    }

    @DeleteMapping("/{id}")
    public Produto deletarUnitariamente(@PathVariable Integer id) {
        Optional<Produto> optional = produtoService.buscarPorId(id);
        if (optional.isPresent()) {
            produtoService.deletar(optional.get());
            return optional.get();
        }
        throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
    }


}
