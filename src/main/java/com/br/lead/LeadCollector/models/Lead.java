package com.br.lead.LeadCollector.models;

import com.br.lead.LeadCollector.enums.TipoDeLead;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
public class Lead {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "nome_completo")
    @Size(min = 8, max = 100, message = "O nome deve ter no minimo 8 caracteres e no maximo 100")
    private String nome;

    @Email(message = "O formato do email é invalido")
    private String email;
    private TipoDeLead tipoDeLead;

    @ManyToMany
    private List<Produto> produtos;

    public Lead() {
    }

    public Lead(int id, String nome, String email, TipoDeLead tipoDeLead, List<Produto> produtos) {
        this.id = id;
        this.nome = nome;
        this.email = email;
        this.tipoDeLead = tipoDeLead;
        this.produtos = produtos;
    }

    public List<Produto> getProdutos() {
        return produtos;
    }

    public void setProdutos(List<Produto> produtos) {
        this.produtos = produtos;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public TipoDeLead getTipoDeLead() {
        return tipoDeLead;
    }

    public void setTipoDeLead(TipoDeLead tipoDeLead) {
        this.tipoDeLead = tipoDeLead;
    }
}
