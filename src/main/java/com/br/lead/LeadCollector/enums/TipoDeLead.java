package com.br.lead.LeadCollector.enums;

public enum TipoDeLead {
    QUENTE,
    ORGANICO,
    FRIO
}
