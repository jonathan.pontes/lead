package com.br.lead.LeadCollector.services;

import com.br.lead.LeadCollector.models.Lead;
import com.br.lead.LeadCollector.models.Produto;
import com.br.lead.LeadCollector.repositories.LeadRepository;
import com.br.lead.LeadCollector.repositories.ProdutoRepository;
import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class LeadService {

    @Autowired
    private LeadRepository leadRepository;

    @Autowired
    private ProdutoRepository produtoRepository;

    public Iterable<Produto> buscarTodosProdutos(List<Integer> produtosId){
        Iterable<Produto> produtosIterable = produtoRepository.findAllById(produtosId);
        return produtosIterable;
    }

    public Optional<Lead> buscarPorId(int id){
        Optional<Lead> leadOptional = leadRepository.findById(id);
        return leadOptional;
    }

    public Lead salvarLead(Lead lead){
        Lead leadObjeto = leadRepository.save(lead);
        return leadObjeto;
    }

    public Iterable<Lead> buscarTodosLeads(){
        Iterable<Lead> leads = leadRepository.findAll();
        return leads;
    }

    public Lead atualizarLead(Lead lead) throws ObjectNotFoundException {
        Optional<Lead> leadOptional = buscarPorId(lead.getId());
        if (leadOptional.isPresent()){
            Lead leadObjeto = leadRepository.save(lead);
            return leadObjeto;
        }
        throw new ObjectNotFoundException(LeadService.class, "O lead não foi encontrato");

    }

    public void deletarLead(Lead lead){
        leadRepository.delete(lead);
    }


}
