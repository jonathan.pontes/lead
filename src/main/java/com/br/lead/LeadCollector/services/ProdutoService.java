package com.br.lead.LeadCollector.services;

import com.br.lead.LeadCollector.models.Produto;
import com.br.lead.LeadCollector.repositories.ProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ProdutoService {

    @Autowired
    private ProdutoRepository produtoRepository;

    public Optional<Produto> buscarPorId(int id){
       Optional<Produto> retorno = produtoRepository.findById(id);
        return retorno;
    }

    public Produto cadastrar(Produto produto){
        Produto retorno = produtoRepository.save(produto);
        return retorno;
    }
    public Iterable<Produto> buscarTodos(){
        Iterable<Produto> produtos = produtoRepository.findAll();
        return produtos;
    }
    public Produto atualizar(Produto produto){
        Optional<Produto> optional = buscarPorId(produto.getId());
        if (optional.isPresent()){
            Produto produtoData = optional.get();

            if(produto.getDescricao() == null){
                produto.setDescricao(produtoData.getDescricao());
            }
            if(produto.getNome() == null){
                produto.setNome(produtoData.getNome());
            }
            if(produto.getPreco() == null){
                produto.setPreco(produtoData.getPreco());
            }
        }
        Produto produtoSalvo = produtoRepository.save(produto);
        return produtoSalvo;
    }


    public void deletar(Produto produto){
        produtoRepository.delete(produto);
    }

}
