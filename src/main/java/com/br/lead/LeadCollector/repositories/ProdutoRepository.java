package com.br.lead.LeadCollector.repositories;

import com.br.lead.LeadCollector.models.Produto;
import org.springframework.data.repository.CrudRepository;

public interface ProdutoRepository extends CrudRepository<Produto, Integer> {
}
