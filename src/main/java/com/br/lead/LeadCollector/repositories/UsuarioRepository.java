package com.br.lead.LeadCollector.repositories;

import com.br.lead.LeadCollector.models.Usuario;
import org.springframework.data.repository.CrudRepository;

public interface UsuarioRepository extends CrudRepository<Usuario, Integer> {

    Usuario findByEmail(String email);
}
