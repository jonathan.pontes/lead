package com.br.lead.LeadCollector.repositories;

import com.br.lead.LeadCollector.models.Lead;
import org.springframework.data.repository.CrudRepository;

public interface LeadRepository extends CrudRepository<Lead, Integer> {
}
